# General
Arduino code to measure speed.

## Notes
The following provides general information and suggestions.

### Remove modemmanager
Ubuntu has a package called `modemmanager` preinstalled, which locks the ttyACM ports on the Jetson board. Remove the package to obtain full control.

```
sudo apt -y remove modemmanager
```

### Accessing serial
To flash the Arduino, it is necessary for some linux dustributions to add the user to specific groups. Please refer to your distribution's manual for further information.

## Editor
Atom: https://atom.io/  
Atom Plugin: http://docs.platformio.org/en/latest/ide/atom.html  
Rosserial Arduino: http://platformio.org/lib/show/1634/Rosserial%20Arduino%20Library/installation  
